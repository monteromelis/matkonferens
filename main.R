## PRE-PROCESSING

# load file
enkater <- read.csv("matenkater2012.csv")

# Convert ordered factors to ordered-type
enkater[c('Aalder', 'Bott', 'HurOfta')] <- 
  lapply(enkater[c('Aalder', 'Bott', 'HurOfta')], ordered)
# Levels in $Bott in right order:
enkater$Bott <- ordered(enkater$Bott, levels = c("--1", "1--3", "4--8", ">8"))
# Put levels in $HurOfta in the right order adding level '1gg/termin'. 
enkater$HurOfta <-
  ordered(enkater$HurOfta, levels = c("1gg/termin", levels(enkater$HurOfta)))
# Add level 'Nej' to $Ater (NB: doesn't appear in the survey but is a possible value)
enkater$Ater <- factor(enkater$Ater, levels= c("Ja", "Nej"))
# Add all possible levels to $Hinder. More than one answer coded as "99"
factor(enkater$Hinder, levels = c("a. Inte gott", "b. Onyttig", "c. Dyr", "d. For 
stokigt", "e. Fel oppetider", "f. Inte hemma i tid", "g. Annat", "99", "Ingenting"))

# Create a vector with those questions that are for adults only
vuxFragor <- grep("vux", names(enkater))
