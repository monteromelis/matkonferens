## Prepare the data set to be analysed

# Create a data frame 'var' containing only the variables
# containing the rating of statements on likert scale (agree--don't agree)
var <- var_kva

# We need to replace NAs with the mean for each variable
f1 <- function(vec) {
  m <- mean(vec, na.rm = TRUE)
  vec[is.na(vec)] <- m
  return(vec)
}

var <- as.data.frame(apply(var,2,f1))

# Which statments should be taken out (since needed: nb observ > nb variables)
mydata <- var
# 'mydata' will be the data frame we'll work on

## From Quick-R 'Principal Components and Factor Analysis'
# Pricipal Components Analysis
# entering raw data and extracting PCs
# from the correlation matrix
fit <- princomp(mydata, cor=TRUE)
summary(fit) # print variance accounted for
loadings(fit) # pc loadings
plot(fit,type="lines") # scree plot
fit$scores # the principal components
biplot(fit) 
