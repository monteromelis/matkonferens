% Matkonferensen: diskussion kring veg--köttätarkategorierna i huset (utkast)
% Guio och Carita


# Argument och fakta kring kött

- Klimat-och-miljöargument: fisk och köttindustrin.
- Ekonomisk fråga: dyrare att köpa kött av bra kvalité, t.ex. ekologisk, närproducerad.
- Flera i huset som gärna vill äta mindre kött (?) -- vad säger enkäterna?


# Ställningstagande (om oss)

Vad är vår utgångspunkt i denna fråga? Vad är vi: köttätare, vegetarianer?

Både Carita och jag är inte vegetarianer, eftersom vi äter kött. Vi är ganska överens om att det är gott med visst kött då och då, att det även är nyttigt att äta kött. Däremot behöver vi inte stora mängder av kött och vi behöver inte heller äta det varje dag.
Av bl.a. ovannämnda skäl tycker vi att det finns en poäng i att äta mindre kött och fisk, då både dessa idustrier bär tunga miljömässiga nackdelar med sig.
Men i huset blir man tvungen att välja mellan två kategorier, köttätare eller vegetarian, antingen det ena eller det andra, men ingenting mitt emellan.
Carita har valt att ingå i köttätarekategorin, jag räknas bland vegarna. Men i själva verket tycker vi att det är tråkigt, att vi ofta går miste om något gott, därför att det finns dessa två rigida kategorier hos oss.

Vi kanske inte är de enda som tycker så?



# Tankar kring rigida köttätare--veg kategorier


Man kan fråga sig, varför finns dessa två kategorier? För att skydda vegarna från köttet? För att skydda köttätare från vegmaten? Är det den lösning som passar alla bäst? Är det inte roligare att kunna välja bland olika rätter när man ändå lagat dem?

Vi har fått uppfattningen att:

- Flera vill äta av det vegetariska alternativet men de gör det inte för att den är bara till för vegetarianerna
- Flera som skulle vilja äta mindre kött allmänt; det kanske skulle räcka en eller två gånger i veckan; men de vill inte ge upp köttet helt och hållet
- I nuläget är vegetarisk mat "undantagsmaten". Fastän vi äter utan kött ganska ofta, vet vi mycket mindre om veg-recept än om köttrecept
- Fler vegetariska recept efterfrågas (enkäterna!) -- men ska alla andra då missa dessa goda recept? Ska bara veggarna äta dem?



# Vad behövs?

Utöka kunskapsbasen kring vegetarisk mat:

- Fler vegetariska rätter
- Mer sallad: salladbuffé och recept
- Ta hit någon som är duktig; matlagningskurs? 
- Inspirationsdag, särskilt för matgruppen!
- Våga chansa lite mer -- raggmunk med vegetarisk röra, kan det vara gott? Ja!

Man behöver också vara lite mer flexibel kanske...


# Andra fördelar

- Mindre jobb att för det mesta bara laga en enda rätt
- När det finns kött så kan man äta två olika rätter, vilken lyx!



# Ett möjligt förslag

Laga hälften veg hälften kött, när det är kött.



# Diskussionsfrågor


- Är jag beredd att äta vegetarisk rätt om jag kommer för sent och köttalternativet tog slut?
- När man har kötträtter så ska man laga hälften med kött och hälften utan (förslag).
- Är jag som vegetarian beredd att äta sallader och lite annat om den vegetariska rätten har tagit slut?
- Idéer kring hur man kan utveckla rätter utan kött? 
- Vad tycker du själv är goda rätter utan kött?
- Vilka argument finns mot vegetarisk mat?
- Är det svårare för barn att äta vegetarisk mat?


# Att göra

- Vegetarisk kollektivhuskokbok (med recept för 30 pers)
- Prata med andra kollektivhus, se hur de gör, få inspiration
- Ordna en kurs, inspirationsdag, studiecirkel?


# Möjliga kontakter

- Facebookgruppen om mat i kollektivhus
- Kollektivhus i Hökarängen (Cigarrlådan)
