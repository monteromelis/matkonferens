% README
% Guillermo Montero Melis

# Allmänt

Här finns dokument inför matkonferensen 2012 i kollektivhuset Fullersta Backe.
Huvudmaterialet här är resultat av matenkäterna som bearbetas i R.
Data finns även i *csv* format.
