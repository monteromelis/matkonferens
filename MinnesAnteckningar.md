% Minnesanteckningar från matkonferensen 2012
% Guio, Ode
% 2012-11-18



# Barnens idéer och synpunkter. (Ansvarig: ....)

- Ihopfällbart bord, då det finns platsbrist ibland på fredagar
- Barnens matvecka (uppskattad av barnen!) -- kommer i januari!
- Ibland bröd + och smaskig pålägg som komplement
- Ungdomarnas och barnens åsikter -- föräldrarna kan lägga förslag i förslagslådan


# Redovisning av enkäten. (Ansvariga: Guio och John)

- Rapporten ska skickas via mejl till alla i huset
- Med i rapporten: hur många procent svarade bland vuxna?
- Jämföra svar till HurOfta med Ingvars statistik om hur ofta man faktiskt äter
- Kluriga påståenden: påst 14 är svårt tolkad; undvika "inte"frågor ("Inte inte-frågor")


# Annika från Sockenstugan. (Ansvarig: John?)

- Matpolicy -- använda deras som utgångspunkt (med förändringar om behov finns)
- Ta del av alla deras matrecept
- De har en stor ekologisk leverantör: Allt i Grönt
- Vi har blivit inbjudma till Sockenstugan
- Deras system var intressant och ger impulser; allmänt sett kräver det dock mer organisation -- vad kan vi lära oss?
- Skicka  enkäten + slutrapporten till Annika


# Diskussion kring Ramverket. Ställa fram matpolicy (Ansvarig: Lena och Tove)

- Förslag till husmöte: Det ska finnas möjlighet att be matlagarna att ställa undan mat när man inte hinner hem i tid (dvs före kl 19) till vanlig pris
- Riktlinjer för matgruppen
	- Förslag: Möte med matlagarna veckan innan så att de blir mer inblandade i matlagning/planering (hur realistiskt är det?)
	- Riktar sig efter matpolicy (se punkt "Annika från Sockenstugan")
	- Flera möten i matgruppen, bättre koordination
	- Leverantörer, särskilt ekologiska sådana: Allt i Grönt (se punkt "Annika från Sockenstugan")?
	- Matreceptbank, både i pärmar och på hemsidan
	- Receptbank även för sallader
	- Mer utförlig information om sallad till matlagare
- Tydligare riktlinjer för matlagare
	- Städning i matsalen
	- Städa köket mer noggrant: vad exakt ska städas?
- Matlagare ansvarar för att det finns mat fram till kl 19 -- detta gäller även sallad!



# Diskussioner kring ekologi mm och kvalité mm (Ansvariga: John och Marke)


## Ekologi närhet och säsong (Ansvarig: John)

Sammanfattas av John


## Matens kvalité (Ansvarig: Marke)

Sammanfattas av Marke



# Diskussion kring veg- och köttätarkategorier i huset. (Ansvarig: ...)

